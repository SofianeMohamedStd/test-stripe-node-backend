const express = require("express");
const app = express();
// This is a sample test API key. Sign in to see examples pre-filled with your key.
const stripe = require("stripe")("sk_test_51IMYwRHdeqTXU8MUEelRoEJWIQ3emdEOYvSyIGSz0OO9aTEMMBnl8IBFb3X8BJ1v2sU39w5MQDzQGYI4vAW6Grli00sHpajrkL");

app.use(express.static("."));
app.use(express.json());

const calculateOrderAmount = items => {
  // Replace this constant with a calculation of the order's amount
  // Calculate the order total on the server to prevent
  // people from directly manipulating the amount on the client
  return 1400;
};

app.post("/create-payment-intent", async (req, res) => {
  const { items } = req.body;
  // Create a PaymentIntent with the order amount and currency
  const paymentIntent = await stripe.paymentIntents.create({
    amount: calculateOrderAmount(items),
    currency: "usd"
  });

  res.send({
    clientSecret: paymentIntent.client_secret
  });
});

app.get("/greet", async (req, res) => {
    res.send("Hello, it is working");
  });
app.listen(4242, () => console.log('Node server listening on port 4242!'));

